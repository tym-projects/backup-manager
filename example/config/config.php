<?php

return [
    'backup' => [
        'databases' => [
            [
                'type' => 'mysql',
                'host' => 'localhost',
                'user' => 'user',
                'password' => 'password',
                'name' => 'database1',
            ],
            //...
        ],
        'folders' => [
            __DIR__.'/data',
            //...
        ],
    ],
    'storages' => [
        'local' => [
            'folder' => '/local-folder',
        ],
        'awss3' => [
            'key' => '00000000000',
            'api_secret' => 'klaksjflkasjflaksjflkasfj',
            'region' => 'es',
            'name' => 'spacename',
            'remote' => '/backups/customer_id',
        ],
    ],
];
