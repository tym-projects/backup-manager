<?php
declare (strict_types = 1);

use BackupManager\BackupManager;

require_once '../vendor/autoload.php';

BackupManager::buildFrom(
    require __DIR__ . '/config/config.php'
)->backup();

BackupManager::buildFrom(
    require __DIR__ . '/config/config.php'
)->restore("2021-09-01_00-00-00.zip");
