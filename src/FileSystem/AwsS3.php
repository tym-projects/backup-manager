<?php

declare (strict_types = 1);

namespace BackupManager\FileSystem;

use BackupManager\FileSystem\FileSystemInterface;
use Exception;
use LogicException;
use SpacesAPI\Exceptions\FileDoesntExistException;
use SpacesAPI\Exceptions\SpaceDoesntExistException;
use SpacesAPI\File;
use SpacesAPI\Space;
use SpacesAPI\Spaces;

class AwsS3 implements FileSystemInterface
{

    const ARRAY_KEYS = ['key', 'api_secret', 'region', 'name', 'remote'];

    private $space;

    private $remote;

    /**
     * @param Space $space
     * @return void
     */
    public function __construct(
        Space $space,
        ?string $remote = null
    ) {
        $this->space = $space;
        $this->remote = $remote;
    }

    /**
     * @param array $config
     * @return AwsS3
     * @throws Exception
     * @throws SpaceDoesntExistException
     */
    public static function from(array $config): self
    {
        foreach (self::ARRAY_KEYS as $key) {
            if (!array_key_exists($key, $config)) {
                throw new Exception(
                    sprintf('key %s not found in config file for %s', $key, self::class)
                );
            }
        }

        return new self(
            (new Spaces($config['key'], $config['api_secret'], $config['region']))->space($config['name']),
            ($config['remote']) ?? ''
        );
    }

    /**
     * @param string $remote
     * @return array
     */
    public function getFiles(string $remote = ''): array
    {
        return $this->space->listFiles($remote)['files'];
    }

    /**
     * @param string $path
     * @return File
     * @throws Exception
     */
    public function saveFile(string $path): File
    {
        if (!is_file($path)) {
            throw new Exception(
                sprintf('File %s not found', $path)
            );
        }

        return $this->space->uploadFile($path);
    }

    /**
     * @param string $source
     * @param string|null $remote
     * @return void
     * @throws Exception
     * @throws LogicException
     */
    public function saveDirectory(string $source, string $remote = null)
    {
        if (!is_dir($source)) {
            throw new Exception(
                sprintf('Folder %s not found', $source)
            );
        }
        return $this->space->uploadDirectory($source, ($remote) ?? $this->remote);
    }

    /**
     * @param string $filename
     * @param string $destination
     * @return bool
     * @throws FileDoesntExistException
     * @throws Exception
     */
    public function getFile(string $filename, string $destination): bool
    {

        $file = $this->space->file($this->remote . "/" . $filename);

        $file->download($destination . "/" . $filename);

        if (!\is_file($destination . "/" . $filename)) {
            throw new Exception(
                sprintf('File %s not found', $destination . "/" . $filename)
            );
        }

        return true;

    }
}
