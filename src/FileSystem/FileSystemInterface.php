<?php

namespace BackupManager\FileSystem;

interface FileSystemInterface
{

    public function saveDirectory(string $source, string $remote = null);

    public function getFile(string $filename, string $destination): bool;

}
