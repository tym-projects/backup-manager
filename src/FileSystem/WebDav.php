<?php

declare (strict_types = 1);

namespace BackupManager\FileSystem;

use BackupManager\FileSystem\FileSystemInterface;
use DOMException;
use Exception;
use InvalidArgumentException;
use Sabre\DAV\Client;
use Sabre\HTTP\ClientException;
use Sabre\HTTP\ClientHttpException;
use Sabre\Uri\InvalidUriException;

class WebDav implements FileSystemInterface
{

    const ARRAY_KEYS = ['host', 'user', 'password'];
    const DEFAUL_FOLDER = 'files';

    /**
     * @var Client
     */
    public function __construct(
        private Client $client,
        private array $config,
    ) {
    }

    /**
     * @param array $config
     * @return WebDav
     * @throws Exception
     */
    public static function from(array $config): self
    {
        foreach (self::ARRAY_KEYS as $key) {
            if (!array_key_exists($key, $config)) {
                throw new Exception(
                    sprintf('key %s not found in config file for %s', $key, self::class)
                );
            }
        }

        return new self(
            new Client([
                'baseUri' => $config['host'],
                'userName' => $config['user'],
                'password' => $config['password'],
            ]), $config
        );
    }

    /**
     * @param string $remote
     * @return array
     * @throws DOMException
     * @throws InvalidArgumentException
     * @throws InvalidUriException
     * @throws ClientException
     * @throws ClientHttpException
     */
    public function getFiles(string $remote = ''): array
    {

        return $this->client->propFind(
            self::DEFAUL_FOLDER . $remote,
            [
                '{DAV:}getlastmodified',
                '{DAV:}getcontentlength',
                '{DAV:}getcontenttype',
                '{DAV:}resourcetype',
                '{DAV:}displayname',
            ], 1
        );
    }

    /**
     * @param string $remote
     * @return bool
     * @throws Exception
     */
    public function saveFile(string $path, string $remote = null)
    {
        if (!is_file($path)) {
            throw new Exception(
                sprintf('File %s not found', $path)
            );
        }

        $remote = $remote ?? $this->config['remote'];

        $this->createFolderRecursively($remote);

        $response = $this->client->request(
            'PUT',
            sprintf('%s/%s/%s', self::DEFAUL_FOLDER, $remote, basename($path)),
            file_get_contents($path)
        );

        if ($response['statusCode'] !== 201) {
            throw new Exception(
                sprintf('Error saving file %s. Server responded with status code %d', $path, $response['statusCode'])
            );
        }

        return true;
    }

    /**
     * @param string $source
     * @param string|null $remote
     * @return void
     * @throws Exception
     */
    public function saveDirectory(string $source, string $remote = null)
    {
        if (!is_dir($source)) {
            throw new Exception(
                sprintf('Folder %s not found', $source)
            );
        }

        $remote = $remote ?? $this->config['remote'];

        foreach (scandir($source) as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            if (is_dir($source . '/' . $file)) {
                $this->saveDirectory($source . '/' . $file, $remote . '/' . $file);
            } else {
                $this->saveFile($source . '/' . $file, $remote);
            }
        }

    }

    /**
     * @param mixed $folderPath
     * @return void
     */
    private function createFolderRecursively($folderPath)
    {
        $folders = explode('/', $folderPath);
        $currentPath = self::DEFAUL_FOLDER;
        foreach ($folders as $folder) {
            $currentPath .= '/' . $folder;
            $this->client->request('MKCOL', $currentPath);
        }
    }

    /**
     * @param string $filename
     * @param string $destination
     * @return bool
     */
    public function getFile(string $filename, string $destination): bool
    {
        try {

            $response = $this->client->request('GET', self::DEFAUL_FOLDER . $this->config['remote'] . '/' . $filename);

            if ($response['statusCode'] !== 200) {
                throw new Exception(sprintf('Error downloading file %s. Server responded with status code %d', $filename, $response['statusCode']));
            }

            if (file_put_contents($destination . '/' . $filename, $response['body']) === false) {
                throw new Exception(sprintf('Error saving file to %s', $destination));
            }

            return true;
        } catch (Exception $e) {
            error_log('Error in getFile: ' . $e->getMessage());
            return false;
        }
    }
}
