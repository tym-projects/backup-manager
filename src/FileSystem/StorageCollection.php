<?php

namespace BackupManager\FileSystem;

use ArrayIterator;
use BackupManager\FileSystem\FileSystemInterface;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;

class StorageCollection implements Countable, IteratorAggregate
{

    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(array $items)
    {
        foreach ($items as $item) {
            if (!$item instanceof FileSystemInterface) {
                throw new InvalidArgumentException(
                    sprintf("%s is not an %s implementation", get_class($item), FileSystemInterface::class)
                );
            }
        }
        $this->items = $items;
    }

    /**
     * @param mixed $key
     * @return null|FileSystemInterface
     */
    public function get($key): ?FileSystemInterface
    {
        return (isset($this->items[$key]))
        ? $this->items[$key]
        : null;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

}
