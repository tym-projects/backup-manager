<?php

namespace BackupManager\FileSystem;

use BackupManager\Utils\Directory;
use Exception;
use InvalidArgumentException;

class LocalStorage implements FileSystemInterface
{

    const ARRAY_KEYS = ['folder'];

    private $folder;

    /**
     * @param string $folder
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(string $folder)
    {
        if (!is_dir($folder)) {
            throw new InvalidArgumentException(
                sprintf('%s folder not found', $folder)
            );
        }
        $this->folder = $folder;
    }

    /**
     * @param array $config
     * @return LocalStorage
     * @throws Exception
     */
    public static function from(array $config): self
    {
        foreach (self::ARRAY_KEYS as $key) {
            if (!array_key_exists($key, $config)) {
                throw new Exception(
                    sprintf('key %s not found in config file for %s', $key, self::class)
                );
            }
        }

        return new self($config['folder']);
    }

    /**
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param string $source
     * @param string|null $remote
     * @return void
     */
    public function saveDirectory(string $source, string $remote = null)
    {
        Directory::copy(
            $source,
            $this->folder
        );
    }

    /**
     * @param string $filename
     * @param string $destination
     * @return bool
     */
    public function getFile(string $filename, string $destination): bool
    {

        if (!is_dir($destination)) {
            throw new InvalidArgumentException(
                sprintf('destination folder \'%s\' not found', $destination)
            );
        }

        if (is_file($this->folder . "/" . $filename)) {
            copy($this->folder . "/" . $filename, $destination . "/" . $filename);
            return true;
        }
        return false;
    }

}
