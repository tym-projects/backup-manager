<?php

namespace BackupManager\Utils;

final class Directory
{

    /**
     * @param string $source
     * @param string $destination
     * @return void
     */
    public static function copy(
        string $source,
        string $destination
    ) {
        foreach (scandir($source) as $file) {
            if (!is_readable($source . DIRECTORY_SEPARATOR . $file) || $file == '.' || $file == '..') {
                continue;
            }

            if (is_dir($source . DIRECTORY_SEPARATOR . $file) && ($file != '.') && ($file != '..')) {
                mkdir($destination . DIRECTORY_SEPARATOR . $file);
                self::copy($source . DIRECTORY_SEPARATOR . $file, $destination . DIRECTORY_SEPARATOR . $file);
            } else {
                copy($source . DIRECTORY_SEPARATOR . $file, $destination . DIRECTORY_SEPARATOR . $file);
            }
        }
    }

    /**
     * @param string $directory
     * @return bool
     */
    public static function delete(string $directory, bool $deteteSelf = false): bool
    {
        $files = array_diff(scandir($directory), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$directory/$file"))
            ? self::delete("$directory/$file", true)
            : unlink("$directory/$file");
        }
        return ($deteteSelf) ? rmdir($directory) : true;
    }
}
