<?php

namespace BackupManager\Utils;

use Exception;
use ZipArchive;

/**
 * @testFunction testCompressorDecompressor
 */
class Decompressor
{

    public static function run(
        string $path,
        string $destination
    ): bool {
        if (!file_exists($path)) {
            throw new Exception(
                sprintf('%s not exist', $path)
            );
        }

        if (!is_readable($destination)) {
            throw new Exception(
                sprintf('%s is not readable', $destination)
            );
        }

        $zip = new ZipArchive();

        if (!$zip->open($path)) {
            throw new Exception(
                sprintf("Can not open zip archive")
            );
        }

        if (!$zip->extractTo($destination)) {
            throw new Exception(
                sprintf('can not decompress %s in $s folder', $path, $destination)
            );
        };

        return $zip->close();
    }
}
