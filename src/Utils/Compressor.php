<?php

declare (strict_types = 1);

namespace BackupManager\Utils;

use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

/**
 * @testFunction testCompressorDecompressor
 */
class Compressor
{

    /**
     * @param string $path
     * @param string $destination
     * @return bool
     * @throws Exception
     */
    public static function run(
        string $path,
        string $destination,
        bool $includeFolder = false
    ): bool {

        if (!file_exists($path)) {
            throw new Exception(
                sprintf('%s not exist', $path)
            );
        }

        if (!is_readable($path)) {
            throw new Exception(
                sprintf('%s is not readable', $path)
            );
        }

        if (!is_readable(pathinfo($destination)['dirname'])) {
            throw new Exception(
                sprintf('%s is not readable', pathinfo($destination)['dirname'])
            );
        }

        return (is_dir($path))
        ? self::compressFolder($path, $destination, $includeFolder)
        : self::compressFile($path, $destination);

    }

    /**
     * @param string $file
     * @param string $destination
     * @return bool
     * @throws Exception
     */
    private static function compressFile(
        string $file,
        string $destination
    ): bool {
        $zip = new ZipArchive();
        $result = $zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        if ($result !== true) {
            throw new Exception(
                sprintf('Could not create a %s file', basename($destination))
            );
        }
        $zip->addFile($file, basename($file));
        return $zip->close();
    }

    private static function compressFolder(
        string $path,
        string $destination,
        bool $includeFolder
    ): bool {

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        $zip = new ZipArchive();

        if ($zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== true) {
            throw new Exception(
                sprintf('Could not create a %s file', basename($destination))
            );
        }

        foreach ($files as $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();

                $relativePath = ($includeFolder)
                ? substr($filePath, strpos($filePath, realpath($path)))
                : substr($filePath, strpos($filePath, realpath($path)) + strlen(realpath($path)));
                $zip->addFile($filePath, $relativePath);
            }
        }

        return $zip->close();
    }
}
