<?php

namespace BackupManager\LocalFolder;

use Exception;
use InvalidArgumentException;

class Folder
{
    private $folder;

    /**
     * @param string $folder
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(string $folder)
    {
        if (!is_dir($folder)) {
            throw new InvalidArgumentException(
                sprintf('%s folder not found', $folder)
            );
        }

        $this->folder = $folder;
    }

    /**
     * @param array $config
     * @return LocalStorage
     * @throws Exception
     */
    public static function from(string $folder): self
    {

        if (!is_dir($folder)) {
            throw new Exception(
                sprintf('%s folder not exists', $folder)
            );
        }

        return new self($folder);
    }

    public function getFolderName(): string
    {
        $f = explode(DIRECTORY_SEPARATOR, $this->folder);
        return $f[sizeof($f) - 1];
    }

    /**
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

}
