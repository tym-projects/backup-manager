<?php

namespace BackupManager\LocalFolder;

use ArrayIterator;
use BackupManager\LocalFolder\Folder;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;

class LocalFolderCollection implements Countable, IteratorAggregate
{

    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(array $items)
    {
        foreach ($items as $item) {
            if (!$item instanceof Folder) {
                throw new InvalidArgumentException(
                    sprintf("%s is not an %s implementation", get_class($item), Folder::class)
                );
            }
        }
        $this->items = $items;
    }

    /**
     * @param mixed $key
     * @return null|FileSystemInterface
     */
    public function get($key): ?Folder
    {
        return (isset($this->items[$key]))
        ? $this->items[$key]
        : null;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

}
