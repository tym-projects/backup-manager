<?php

namespace BackupManager;

use BackupManager\FileSystem\AwsS3;
use BackupManager\FileSystem\LocalStorage;
use BackupManager\FileSystem\StorageCollection;
use BackupManager\FileSystem\WebDav;
use InvalidArgumentException;

class StorageProvider
{

    /**
     * @param null|array $storages
     * @return StorageCollection
     */
    public static function buildCollectionFrom(?array $storages)
    {
        $array = [];

        if ($storages) {
            foreach ($storages as $key => $config) {
                switch ($key) {
                    case 'local':
                        $array[] = LocalStorage::from($config);
                        break;
                    case 'awss3':
                        $array[] = AwsS3::from($config);
                        break;
                    case 'webdav':
                        $array[] = WebDav::from($config);
                        break;
                    default:
                        throw new InvalidArgumentException(
                            sprintf('%s driver not valid for storage', $key)
                        );
                }
            }
        }

        return new StorageCollection($array);
    }
}
