<?php

namespace BackupManager;

use BackupManager\LocalFolder\Folder;
use BackupManager\LocalFolder\LocalFolderCollection;

class LocalFolderProvider
{

    /**
     * @param null|array $folders
     * @return LocalFolderCollection
     */
    public static function buildCollectionFrom(?array $folders)
    {
        $array = [];

        if ($folders) {
            foreach ($folders as $folder) {
                $array[] = Folder::from($folder);
            }
        }

        return new LocalFolderCollection($array);
    }
}
