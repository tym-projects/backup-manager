<?php

namespace BackupManager;

use BackupManager\Database\DatabaseCollection;
use BackupManager\Database\MariadbDatabaseManager;
use BackupManager\Database\MysqlDatabaseManager;
use InvalidArgumentException;

class DatabaseProvider
{

    /**
     * @param null|array $storages
     * @return DatabaseCollection
     */
    public static function buildCollectionFrom(?array $databases)
    {
        $array = [];

        if ($databases) {
            foreach ($databases as $config) {
                switch ($config['type']) {
                    case 'mysql':
                        $array[] = MysqlDatabaseManager::from($config);
                        break;
                    case 'mariadb':
                        $array[] = MariadbDatabaseManager::from($config);
                        break;
                    default:
                        throw new InvalidArgumentException(
                            sprintf('%s driver not valid form database backup', $config['type'])
                        );
                }
            }
        }

        return new DatabaseCollection($array);
    }
}
