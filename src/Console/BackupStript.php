<?php

namespace BackupManager\Console;

use InvalidArgumentException;

class BackupStript
{
    const OPTION_STRING = "f:h";
    const OPTION_ARRAY = [
        'File',
        'Help',
    ];

    /**
     * @var array
     */
    private $config;

    /**
     * @param array $options
     * @return void
     * @throws InvalidArgumentException
     */
    private function __construct(array $options)
    {
        if (isset($options['h']) || empty($options)) {
            $this->writeHelp();
        }

        if (!isset($options['f'])) {
            throw new InvalidArgumentException("Database file argument is required, please put %s in command line" . PHP_EOL, "'-f <file_path>'");
        }

        if (!is_file($options['f'])) {
            throw new InvalidArgumentException("Database file not found in %s" . PHP_EOL, $options['f']);
        }

        //$this->config = InitialConfig::getConfig();

        //$this->process();
    }

    /**
     * @return ProjectInstaller
     * @throws SlimConsoleException
     */
    public static function run(): self
    {
        $options = getopt(self::OPTION_STRING, self::OPTION_ARRAY);
        return new self($options);
    }

    /**
     * @return void
     * @throws SlimConsoleException
     */
    private function process()
    {

    }

    /**
     * @return never
     */
    private function writeHelp()
    {
        fwrite(
            STDOUT,
            "Backup Manager Help" . PHP_EOL .
            "usage: backup -f <path-config-file> [-h] ..." . PHP_EOL . PHP_EOL .
            "parameters:" . PHP_EOL .
            " -f, \tParameter with the relative path and name of the config file " . PHP_EOL .
            " -h, \t(Optional) Show this help" . PHP_EOL . PHP_EOL
        );

        exit();
    }
}
