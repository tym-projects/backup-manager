<?php

namespace BackupManager\Database;

use ArrayIterator;
use BackupManager\Database\DatabaseInterface;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;

class DatabaseCollection implements Countable, IteratorAggregate
{

    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(array $items)
    {
        foreach ($items as $item) {
            if (!$item instanceof DatabaseInterface) {
                throw new InvalidArgumentException(
                    sprintf("%s is not an %s implementation", get_class($item), DatabaseInterface::class)
                );
            }
        }
        $this->items = $items;
    }

    /**
     * @param mixed $key
     * @return null|DatabaseInterface
     */
    public function get($key): ?DatabaseInterface
    {
        return (isset($this->items[$key]))
        ? $this->items[$key]
        : null;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

}
