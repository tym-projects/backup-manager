<?php

namespace BackupManager\Database;

interface DatabaseInterface
{
    public function backup(string $destinationPath = null);

    public function restore(string $filePath);

    public function getDbName(): string;
}
