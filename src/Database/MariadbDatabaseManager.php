<?php

declare (strict_types = 1);

namespace BackupManager\Database;

use Exception;
use PDO;

final class MariadbDatabaseManager implements DatabaseInterface
{
    const ARRAY_KEYS = ['host', 'user', 'password', 'name'];
    const SETTINGS = [
        'driver' => 'mysql',
        'charset' => 'utf8mb4',
    ];

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $dbuser;

    /**
     * @var string
     */
    private $dbpassword;

    /**
     * @var string
     */
    private $dbname;

    /**
     * @var mixed
     */
    private $destinationPath;

    /**
     * @param string $host
     * @param string $dbuser
     * @param string $dbpassword
     * @param string $dbname
     * @return void
     */
    public function __construct(
        string $host,
        string $dbuser,
        string $dbpassword,
        string $dbname,
    ) {
        $this->host = $host;
        $this->dbuser = $dbuser;
        $this->dbpassword = $dbpassword;
        $this->dbname = $dbname;
        $this->destinationPath = sys_get_temp_dir() . "/{$dbname}.sql";
    }

    /**
     * @param array $config
     * @return MysqlDatabaseManager
     * @throws Exception
     */
    public static function from(array $config)
    {
        foreach (self::ARRAY_KEYS as $key) {
            if (!array_key_exists($key, $config)) {
                throw new Exception(
                    sprintf('key %s not found in config file for %s', $key, self::class)
                );
            }
        }

        return new self($config['host'], $config['user'], $config['password'], $config['name']);
    }

    /**
     * @return string
     */
    public function getDestinationPath(): string
    {
        return $this->destinationPath;
    }

    /**
     * @param string|null $destinationPath
     * @return $this
     */
    public function backup(string $destinationPath = null)
    {
        if ($destinationPath) {
            $this->destinationPath = $destinationPath;
        }
        $cmd = "mariadb-dump -h {$this->host} -u{$this->dbuser} -p{$this->dbpassword} {$this->dbname} > {$this->destinationPath}";
        shell_exec($cmd);
    }

    /**
     * @param string|null $filePath
     * @return void
     * @throws Exception
     */
    public function restore(string $filePath)
    {
        if (!is_file($filePath)) {
            throw new Exception(
                sprintf('%s File not found', $filePath)
            );
        }

        $pdo = new PDO(
            self::SETTINGS['driver'] . ":host=" . $this->host . ";charset=" . self::SETTINGS['charset'],
            $this->dbuser,
            $this->dbpassword
        );

        $exists = $this->dbExist($pdo, $this->dbname);
        if ($exists) {
            $pdo->query("DROP DATABASE `{$this->dbname}`");
        }

        $pdo->query("CREATE DATABASE `{$this->dbname}`");
        $pdo->query("use `{$this->dbname}`");

        $this->dumpFileToDatabase($pdo, $filePath);
    }

    /**
     * @param PDO $pdo
     * @return bool
     */
    public function dbExist(PDO $pdo, string $databaseName = null): bool
    {
        $tempDatabaseName = ($databaseName) ?? $this->dbname;
        $sql = "SELECT COUNT(*) as count FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{$tempDatabaseName}'";

        return (bool) $pdo->query($sql)->fetchColumn();
    }

    /**
     * @param PDO $pdo
     * @return void
     */
    private function dumpFileToDatabase(PDO $pdo, string $file)
    {
        $fp = fopen($file, 'r');
        $templine = '';
        while (($line = fgets($fp)) !== false) {
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                $pdo->query($templine);
                $templine = '';
            }
        }
        fclose($fp);
    }

    /**
     * @return string
     */
    public function getDbName(): string
    {
        return $this->dbname;
    }
}
