<?php
namespace BackupManager;

use BackupManager\Database\DatabaseCollection;
use BackupManager\Database\DatabaseInterface;
use BackupManager\FileSystem\StorageCollection;
use BackupManager\LocalFolderProvider;
use BackupManager\LocalFolder\LocalFolderCollection;
use BackupManager\Utils\Compressor;
use BackupManager\Utils\Decompressor;
use BackupManager\Utils\Directory;
use Exception;
use InvalidArgumentException;
use SpacesAPI\Exceptions\SpaceDoesntExistException;

final class BackupManager
{
    private const TEMP_DIRNAME = 'backup';
    private const DB_DIRNAME = 'databases';
    private const FOLDER_DIRNAME = 'folders';

    /**
     * @var true
     */
    private $compression;

    /**
     * @var string
     */
    private $tempPath;

    /**
     * @param DatabaseCollection $databases
     * @param LocalFolderCollection $folders
     * @param StorageCollection $storages
     * @return void
     */
    private function __construct(
        private DatabaseCollection $databases,
        private LocalFolderCollection $folders,
        private StorageCollection $storages
    ) {
        $this->databases = $databases;
        $this->folders = $folders;
        $this->storages = $storages;
        $this->compression = true;
        $this->tempPath = sys_get_temp_dir();
    }

    /**
     * @param array $config
     * @return BackupManager
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws SpaceDoesntExistException
     */
    public static function buildFrom(array $config): self
    {
        return new self(
            DatabaseProvider::buildCollectionFrom(($config['backup']['databases']) ?? null),
            LocalFolderProvider::buildCollectionFrom(($config['backup']['folders']) ?? null),
            StorageProvider::buildCollectionFrom($config['storages'])
        );
    }

    /**
     * @return BackupManager
     */
    public function disableCompression(): self
    {
        $this->compression = false;
        return $this;
    }

    /**
     * @return BackupManager
     */
    public function enableCompresion(): self
    {
        $this->compression = true;
        return $this;
    }

    /**
     * @param string|null $tempPath
     * @return void
     * @throws Exception
     */
    public function backup(string $tempPath = null)
    {
        $name = date('Y-m-d_h-i-s');

        $backupFolder = ($tempPath)
        ? $tempPath . "/" . self::TEMP_DIRNAME
        : $this->tempPath . "/" . self::TEMP_DIRNAME;

        try {
            mkdir($backupFolder . "/" . $name, 0777, true);

            $this->backupDatabases($backupFolder . "/" . $name);
            $this->backupFolders($backupFolder . "/" . $name);

            if ($this->compression) {
                Compressor::run(
                    $backupFolder . "/" . $name,
                    $backupFolder . "/" . $name . ".zip"
                );
                Directory::delete($backupFolder . "/" . $name, true);
            }
            $this->storageBackup($backupFolder);

            Directory::delete($backupFolder, true);

        } catch (Exception $e) {
            Directory::delete($backupFolder, true);
            throw $e;
        }

    }

    public function restore(string $file, string $tempPath = null)
    {
        $tempPath = $tempPath ?? $this->tempPath;

        if (!$this->storageRestore($file, $tempPath)) {
            throw new Exception(sprintf("Backup file '%s' not found", $file));
        }

        $finfo = pathinfo($tempPath . "/" . $file);
        mkdir($tempPath . "/" . $finfo['filename'], 0777, true);

        Decompressor::run(
            $tempPath . "/" . $file,
            $tempPath . "/" . $finfo['filename']
        );

        $this->restoreDatabases($tempPath . "/" . $finfo['filename'] . "/" . self::DB_DIRNAME);
        $this->restoreFolders($tempPath . "/" . $finfo['filename'] . "/" . self::FOLDER_DIRNAME);

        Directory::delete($tempPath);

    }

    /**
     * @param string $backupFolder
     * @return void
     */
    private function backupDatabases(string $backupFolder)
    {
        $dirname = $backupFolder . "/" . self::DB_DIRNAME;

        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }

        /** @var DatabaseInterface $db */
        foreach ($this->databases as $db) {
            $db->backup($dirname . "/" . $db->getDbName() . ".sql");
        }
    }

    /**
     * @param string $backupFolder
     * @return void
     */
    private function backupFolders(string $backupFolder)
    {
        $dirname = $backupFolder . "/" . self::FOLDER_DIRNAME;
        if (!\is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }
        foreach ($this->folders as $folder) {
            mkdir($dirname . "/" . $folder->getFolderName());
            Directory::copy(
                $folder->getFolder(),
                $dirname . "/" . $folder->getFolderName()
            );
        }
    }

    public function restoreDatabases(string $path)
    {
        /** @var DatabaseInterface $db */
        foreach ($this->databases as $db) {
            $db->restore($path . "/" . $db->getDbName() . ".sql");
        }
    }

    public function restoreFolders(string $path)
    {
        foreach ($this->folders as $folder) {
            Directory::delete($folder->getFolder());
            Directory::copy(
                $path . "/" . $folder->getFolderName(),
                $folder->getFolder()
            );
        }
    }

    /**
     * @param string $backupFolder
     * @return void
     */
    private function storageBackup(string $backupFolder)
    {

        foreach ($this->storages as $rep) {
            $rep->saveDirectory($backupFolder);
        }
    }

    /**
     * @param string $filename
     * @param string $destination
     * @return bool
     */
    private function storageRestore(string $filename, string $destination): bool
    {
        foreach ($this->storages as $rep) {
            if ($rep->getFile($filename, $destination)) {
                return true;
            }
        }
        return false;
    }
}
