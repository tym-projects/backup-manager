# Backup Manager  
Backup Manager can do backups from docker volumes, folders or mysql databases into local folder, webdav or awsS3

#### Dependecies  
mysql-client  
php zip extension

#### Install via Composer  

Add Gitlab Repository to your composer.json  file
```
"repositories": [{
    "type": "composer",
    "url": "https://gitlab.com/api/v4/group/12098683/-/packages/composer/packages.json"
}]
```  
``` 
composer require tym-projects/backup-manager
```
#### Use  

Config File Example

config.php  

```
'backup' => [
        // List of databases to be backed up
        'databases' => [
            [
                'type' => 'mysql',
                'host' => 'localhost',
                'user' => 'user',
                'password' => 'password',
                'name' => 'database1',
            ],
            //...
        ],
        // List of folders to be added to the backup
        'folders' => [
            __DIR__.'/data',
            ...
        ],
    ],
    // Destination list of backups
    'storages' => [
        // Local folder in server
        'local' => [                
            'folder' => '/local-folder',
        ],
        // S3 remote storage
        'awss3' => [
            'key' => '00000000000',
            'api_secret' => 'klaksjflkasjflaksjflkasfj',
            'region' => 'es',
            'name' => 'spacename',
            'remote' => '/backups/customer_id', //Remote folder to save backup
        ],
        // WebDav remote storage
        'webdav' => [
            'host' => 'https://your-webdav-host/remote.php/dav',
            'user' => 'user',
            'password' => 'password',
            'remote' => '/backups/customer_id', //Remote folder to save backup
        ]
    ],

```

file.php  

```
<?php declare (strict_types = 1);

use BackupManager\BackupManager;

require_once 'vendor/autoload.php';

try{

    // With Comporesion
    BackupManager::buildFrom(
            require __DIR__.'/path/to/config/file/config.php'
        )->backup();

    // Without Compression
    BackupManager::buildFrom(
            $container->get('settings')['backupmanager']
        )->disableCompression()
         ->backup();

    // Restore Backup
    BackupManager::buildFrom(
            $container->get('settings')['backupmanager']
        )->restore('2021-02-12_08-10-10.zip');

}catch (Exception $e){
    fwrite(
        STDERR,
        $e->getMessage() . " in file " . $e->getFile() . " line " . $e->getLine() . PHP_EOL
    );
}
```
