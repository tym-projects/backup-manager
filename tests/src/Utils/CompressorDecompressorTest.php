<?php
namespace PHPTDD\src\Utils;

use BackupManager\Utils\Compressor;
use BackupManager\Utils\Decompressor;
use Exception;
use PHPUnit\Framework\TestCase;

class CompressorDecompressorTest extends TestCase
{

    const FOLDER_TEST = __DIR__ . "/../../data/files";
    const FILE_TEST = self::FOLDER_TEST . "/text.txt";
    const ZIP_FILES_FOLDER = __DIR__ . "/../../data/zipfiles";
    const OUTPUT_FOLDER = __DIR__ . "/../../data/output";

    /**
     * @return void
     */
    protected function setUp(): void
    {
        mkdir(self::ZIP_FILES_FOLDER);
        mkdir(self::OUTPUT_FOLDER);
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {
        self::delTree(self::OUTPUT_FOLDER);
        self::delTree(self::ZIP_FILES_FOLDER);
    }

    /**
     * @covers BackupManager\Utils\Compressor
     **/
    public function testFileToCompressNotExists()
    {
        $this->expectException(Exception::class);
        Compressor::run(
            self::FOLDER_TEST . "/fakefile.txt",
            self::ZIP_FILES_FOLDER . "/file.zip",
            false
        );
    }

    /**
     * @covers BackupManager\Utils\Compressor
     **/
    public function testFolderToCompressNotExists()
    {
        $this->expectException(Exception::class);
        Compressor::run(
            self::FOLDER_TEST . "folder_not_exists",
            self::ZIP_FILES_FOLDER . "/folder.zip",
            false
        );
    }

    /**
     * @covers BackupManager\Utils\Compressor
     **/
    public function testCompressionDestFolderNotExists()
    {
        $this->expectException(Exception::class);
        Compressor::run(
            self::FOLDER_TEST . "/text.txt",
            self::ZIP_FILES_FOLDER . "/fake_folder/file.zip",
            false
        );
    }

    /**
     * @covers BackupManager\Utils\Decompressor
     **/
    public function testDecompressionDestFolderNotExists()
    {
        $this->expectException(Exception::class);
        Decompressor::run(
            self::ZIP_FILES_FOLDER . "/fake.zip",
            self::OUTPUT_FOLDER . "/fake_folder",
        );
    }

    /**
     * @covers BackupManager\Utils\Compressor
     * @covers BackupManager\Utils\Decompressor
     **/
    public function testCompressorDecompressor()
    {
        Compressor::run(
            self::FOLDER_TEST,
            self::ZIP_FILES_FOLDER . "/folder.zip"
        );

        Compressor::run(
            self::FILE_TEST,
            self::ZIP_FILES_FOLDER . "/file.zip"
        );

        Decompressor::run(
            self::ZIP_FILES_FOLDER . "/file.zip",
            self::OUTPUT_FOLDER
        );

        Decompressor::run(
            self::ZIP_FILES_FOLDER . "/folder.zip",
            self::OUTPUT_FOLDER
        );

        $this->assertFileExists(self::ZIP_FILES_FOLDER . "/folder.zip");
        $this->assertFileExists(self::ZIP_FILES_FOLDER . "/file.zip");
        $this->assertTrue(is_dir(self::OUTPUT_FOLDER));

        $this->assertTrue(is_file(self::OUTPUT_FOLDER . "/text.txt"));
        $this->assertTrue(is_dir(self::OUTPUT_FOLDER . "/folder"));
        $this->assertTrue(is_file(self::OUTPUT_FOLDER . "/folder/file_in_folder.txt"));

    }

    /**
     * @param string $dir
     * @param bool $removeDir
     * @return bool|null
     */
    private static function delTree(string $dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file", true) : unlink("$dir/$file");
        }
        return rmdir($dir);
    }
}
